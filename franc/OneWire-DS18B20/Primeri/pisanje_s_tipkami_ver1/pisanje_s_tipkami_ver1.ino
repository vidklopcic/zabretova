#include <LiquidCrystal.h>
LiquidCrystal lcd(8, 13, 9, 4, 5, 6, 7);
#define right  0
#define up     1
#define down   2
#define left   3
#define enter 4
#define no_button 5
int key;
int current_char = 87;
int lcd_column = 0;
void setup(){
  lcd.begin(16, 2);
  lcd.clear();
}

void loop(){
  int btn = read_buttons();
  switch(btn){
    case 1:
      lcd.print(char(ch_char(1)));
      break;
    case 2:
       lcd.print(char(ch_char(0)));
       break;
    case 0:
      if(lcd_column==16){
        lcd.clear();
        lcd_column = 0;
      }else{
        lcd_column ++;
      }
      break;
    case 3:
      if(lcd_column==0){
        lcd.clear();
        lcd_column = 16;
      }else{
        lcd_column --;
      }
      break;
  }
  lcd.setCursor(0, 2);
  lcd.print(current_char);
  delay(250);
  lcd.setCursor(lcd_column, 0);
}

int read_buttons() {
  while(1){
     key = 100;
     int adc_key_in = analogRead(0);
     if (adc_key_in > 1000) {
       key = no_button;
     }else if (adc_key_in < 50) {
       key = right;
     }else if (adc_key_in < 180) {
       key = up; 
     }else if (adc_key_in < 320) {
       key = down; 
     }else if (adc_key_in < 480) {
       key = left; 
     }else if (adc_key_in < 800) {
       key = enter;
     }
     if(key != 100){
       return key;
     }
  }
}

int ch_char(int dir) {
  if (dir==1){
    if(current_char<122){
      current_char++;
    }else{
      current_char = 97;
    }
  } else if(dir==0){
    if(current_char>97){
      current_char--;
    }else{
      current_char = 122;
    }
  }
  return current_char;
}
