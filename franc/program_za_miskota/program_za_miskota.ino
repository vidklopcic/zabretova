int pwm_out = 3;
int smer_out = 4;
int zavora_in = 4;
int gas_analog_in = 1;
int gas_pri_zaviranju = 20;
int threshold_gasa_za_nazaj = 10;
float k_gas = 255.0 / 1024.0;
int n_gas = 1;

int smer;    // 0...nazaj 1...naprej
int zavora;    // 0...izklopljena 1...vklopljena
int gas;//der
int zaviranje = 0;


void setup() {
  pinMode(pwm_out, OUTPUT);
  pinMode(zavora_in, INPUT);
  pinMode(smer_out, OUTPUT);
  
  Serial.begin(9600);
}

void loop() {
  gas = analogRead(gas_analog_in) * k_gas + n_gas;
  zavora = digitalRead(zavora_in);
  Serial.print(gas);
  Serial.println(smer);
  if (zaviranje && gas < threshold_gasa_za_nazaj) {
    zaviranje = 0;
    smer = 0;
  } 
  
  if (zavora && smer == 1) {
    zaviranje = 1;
  } else if (zavora == 0) {
    smer = 1;
    zaviranje = 0;
  }
  osvezi_outpute();
  
  delay(10);
}

void osvezi_outpute() {
  if (zaviranje) {
    analogWrite(pwm_out, gas_pri_zaviranju);
    digitalWrite(smer_out, 0);
  } else {
    analogWrite(pwm_out, gas);
    digitalWrite(smer_out, smer);
  }
}
