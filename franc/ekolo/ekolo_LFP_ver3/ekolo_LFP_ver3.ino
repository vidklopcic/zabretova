int time = 0;
long tmp;
float reading;
void setup() {
  Serial.begin(9600);
}

void loop() {
  if (millis() % 60000) {
    time += 1;
  }
  reading = analogRead(1)*(5/1024.0)*8.32;
  if (reading < 15){
    Serial.print("napetost je pod 15v; t=");
    Serial.println(time);
    while(1) {
     delay(1000); 
    }
  }
  Serial.print(time);
  Serial.print(" min | ");
  Serial.println(reading);
  delay(60000);
}
