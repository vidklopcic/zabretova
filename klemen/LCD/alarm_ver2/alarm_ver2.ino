#include <LiquidCrystal.h>
LiquidCrystal lcd(8, 13, 9, 4, 5, 6, 7);

int ura = 4;
int minute = 59;
int sekunde = 50;

int ponavljanje = 0;

int ledstate = LOW;
int pin = 13;

int uraA = 9;
int minuteA = 00;

void setup(){
  pinMode (pin, OUTPUT);
  Serial.begin(9600);
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print(uraA);
  lcd.setCursor(2, 0);
  lcd.print(":");
  lcd.setCursor(4,  0);
  lcd.print(minuteA);  
  lcd.setCursor(11, 0);
  lcd.print("Alarm"); 
}

void loop(){
  sekunde++;
  delay (1000);
  lcd.setCursor(0, 1);
  lcd.print(ura);
  lcd.setCursor(2, 1);
  lcd.print(":");
  lcd.setCursor(4,  1);
  lcd.print(minute);
  lcd.setCursor(6, 1);
  lcd.print(":");
  lcd.print(sekunde);
  lcd.setCursor(11, 1);
  lcd.print("Ura");
  if (sekunde == 60){
    sekunde = 0;
    minute++;
    lcd.clear();
  }
  if (minute == 60){
    minute = 0;
    ura++;
    lcd.clear();  
  }
  if (ura == 24){
    ura = 0;
    
  }
  if (ura == uraA && minute == minuteA){
    
    while (ponavljanje == 0){
      lcd.clear(); 
      lcd.setCursor (8, 0);
      lcd.print ("SKROB");    
      lcd.setCursor (8, 1);
      lcd.print("SKROB");
      digitalWrite(pin, HIGH);
    
    
  }
  }
}
