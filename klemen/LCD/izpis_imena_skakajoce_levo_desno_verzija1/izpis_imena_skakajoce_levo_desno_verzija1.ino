#include <LiquidCrystal.h>
LiquidCrystal lcd(8, 13, 9, 4, 5, 6, 7);
int stolpec = 16;
int vrstica = 1;
int hitrost = 500;
char ime[] = "KLEMEN";
int kretnica = 1;                       //1-levo 2-desno

void setup(){
  lcd.begin(16, 2);
  lcd.clear();
 // lcd.setCursor(1, 10);
}


void pomik_v_levo(){
  lcd.setCursor(stolpec, vrstica);                                                  
      lcd.print(ime);
      delay(hitrost);
      stolpec = stolpec - 1;
      vrstica = vrstica - 1;
      lcd.clear();
      lcd.setCursor(stolpec,vrstica);
      lcd.print(ime);
      delay (hitrost);
      stolpec = stolpec - 1;
      vrstica = vrstica + 1;
      lcd.clear();
      lcd.setCursor(stolpec,vrstica);
      lcd.print(ime);
}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//pomik v desno
void pomik_v_desno(){
     lcd.setCursor(stolpec,vrstica);
     lcd.print(ime);
     delay(hitrost);
     stolpec++;
     vrstica++;
     lcd.clear();
     lcd.setCursor(stolpec,vrstica);
     lcd.print(ime);
     delay (hitrost);
     stolpec++;
     vrstica = vrstica - 1;
     lcd.clear();
     lcd.setCursor(stolpec,vrstica);
     lcd.print(ime);
     
  }


void loop(){
  
   if (stolpec == 16){
     kretnica = 1;
     stolpec = 16;
     vrstica = 0;
   }
   if (stolpec == 0){
     kretnica = 2;
     stolpec = 0;
     vrstica = 0;
   }
   
   if (kretnica == 1){
    //****************************************************************************************************************************
   //pomik v levo 
   pomik_v_levo();
      
      //**********************************************************************************************************
      
   }
   
   if (kretnica == 2){
     //pomik v desno
       pomik_v_desno();
   } 
     //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     
     
}

