#include <LiquidCrystal.h>
LiquidCrystal lcd(8, 13, 9, 4, 5, 6, 7);
long time = 0;
long trajanje = 0;
int polmer = 33;
float hitrost = 0;
int magnet_pin = 3;
float pot = 0;
float obseg = 0;
float avg_tok = 0;
void setup()
{
  pinMode(magnet_pin,INPUT);
    obseg = 6.28 * polmer;
  lcd.begin(16, 2);
  lcd.clear();
  lcd.setCursor(1, 10);
}

void loop(){
    hitrostinAH();
    if((millis() - time) > 3000){
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print(0);
      while(true){
        long impuls = pulseIn(magnet_pin, LOW);
        if(impuls != 0){
          time = millis();
          break;
        }
      } 
    }
}

//void ah_in_maxtok(){
  //time = millis - time;
  //tok * time;
  
  //avg_tok = (avg_tok + hitrost)/2;
//}
float napetost(int pin){
  int vrednost = analogRead(2);
  return vrednost * (5. / 1024);
}

void hitrostinAH(){
  if(digitalRead(magnet_pin) == LOW){
    pot = pot + (obseg / 100000);
    trajanje = millis() - time;
    time = millis();
    hitrost = (((2*3.14*polmer)/100.)/(trajanje/1000.))*3.6;
    delay(50);
    lcd.setCursor(0,1);
    lcd.print(hitrost);
    lcd.setCursor(0,0);
    lcd.print("KLEMEN");
    lcd.setCursor(10,1);
    lcd.print("KLEMEN");
    while(digitalRead(magnet_pin) == LOW){
      //povp. tok
      delay(2);
    }
  }
}

