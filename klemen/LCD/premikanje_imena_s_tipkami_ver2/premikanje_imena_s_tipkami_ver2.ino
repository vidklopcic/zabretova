#include <LiquidCrystal.h>
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

int stevec = 0; 
String mis = "M";
String sir = "S";
int stolpecM = 0;
int vrsticaM = 0;
int stolpecS = random(0, 16);
int vrsticaS = random(0, 1);
int lcd_key     = 0;
int adc_key_in  = 0;
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5
void setup(){
  lcd.begin(16, 2);
  lcd.setCursor(stolpecM,vrsticaM);
  lcd.print(mis);
  delay(1000);
  lcd.setCursor(stolpecS, vrsticaS);
  lcd.print(sir);
}
void misjePremikanje(int stolpecM = stolpecM, int vrsticaM = vrsticaM){
  lcd.clear();
  lcd.setCursor(stolpecM, vrsticaM);  
  lcd.print(mis);
  delay (130);
}
void premikanjeSira(){
  lcd.clear();
  stolpecS = random (0, 16);
  vrsticaS = random (0, 1);
  lcd.print(sir);
  delay(130);
  lcd.setCursor(stolpecM, vrsticaM);
  lcd.print(mis);
}
int cas = millis();
void loop(){
  if (cas/1000 == 10){
    lcd.setCursor(0, 0);
    lcd.print("cas je potekel!");
    lcd.setCursor(0, 1);
    lcd.print("pojedel si" + stevec);    
  }
  int read_LCD_buttons();
  adc_key_in = analogRead(A0);
  if (adc_key_in < 70){
    //desno
    misjePremikanje(stolpecM++,vrsticaM);
    lcd.setCursor(stolpecS, vrsticaS);
    lcd.print(sir);
    lcd.setCursor(stolpecM, vrsticaM);
  }
  else if (adc_key_in < 170){
    //gor
    misjePremikanje(stolpecM, vrsticaM = 0);
    lcd.setCursor(stolpecS, vrsticaS);
    lcd.print(sir);
    lcd.setCursor(stolpecM, vrsticaM);
  } 
  else if (adc_key_in < 270){
    //dol
    misjePremikanje(stolpecM, vrsticaM++);
    lcd.setCursor(stolpecS, vrsticaS);
    lcd.print(sir);
    lcd.setCursor(stolpecM, vrsticaM);
  } 
  else if (adc_key_in < 450) {
    //levo
    misjePremikanje(stolpecM--, vrsticaM);
    lcd.setCursor(stolpecS, vrsticaS);
    lcd.print(sir);
    lcd.setCursor(stolpecM, vrsticaM);
  }
 if (stolpecM == 16){
   stolpecM--;
   lcd.print(sir);
   lcd.setCursor(stolpecM, vrsticaM);
 }
 if (stolpecM == stolpecS && vrsticaM == vrsticaS){
    premikanjeSira();
    stevec++;
 }
}

