int pwm_out = 3;
int smer_out = 2;
int zavora_in = 4;
int gas_analog_in = 1;
int gas_pri_zaviranju = 150;
int threshold_gasa_za_nazaj = gas_pri_zaviranju;
float k_gas = 255.0 / 1024.0;
int n_gas = 0;

int smer;    // 0...nazaj 1...naprej
int zavora;    // 0...izklopljena 1...vklopljena
int gas;
int zaviranje = 0;
int prehod_v_naprej = 0;

void setup() {
  pinMode(pwm_out, OUTPUT);
  pinMode(zavora_in, INPUT);
  pinMode(smer_out, OUTPUT);
  
  Serial.begin(9600);
}

void loop() {
  gas = analogRead(gas_analog_in) * k_gas + n_gas;
  zavora = !digitalRead(zavora_in);
  Serial.print("gas: ");
  Serial.print(gas);
  Serial.print("; zaviranje: ");
  Serial.print(zaviranje);
  Serial.print("; smer: ");
  Serial.println(smer);
  if (zaviranje && gas < threshold_gasa_za_nazaj) {
    zaviranje = 0;
    smer = 0;
  }
  
  if (prehod_v_naprej && gas < threshold_gasa_za_nazaj) {
    prehod_v_naprej = 0;
    smer = 1;
  }
  
  if (zavora && smer == 1) {
    zaviranje = 1;
  } else if (zavora == 0 && smer == 0) {
    prehod_v_naprej = 1;
  } else if (zavora == 0) {
    smer = 1;
    zaviranje = 0;
  }
  osvezi_outpute();
  
  delay(10);
}

void osvezi_outpute() {
  if (zaviranje) {
    analogWrite(pwm_out, gas_pri_zaviranju);
    digitalWrite(smer_out, 0);
  } else if (prehod_v_naprej) {   
    analogWrite(pwm_out, gas_pri_zaviranju);
    digitalWrite(smer_out, 1);
  } else {
    analogWrite(pwm_out, gas);
    digitalWrite(smer_out, smer);
  }
}
